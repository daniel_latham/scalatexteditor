import java.awt.Color

import scala.swing._
import scala.swing.event.ButtonClicked

/**
 * Created by Stephen Stucky on 2/7/15.
 *  
 */

object playingWithSwing extends SimpleSwingApplication  {
  def top = new MainFrame {
    contents = new GridPanel(1,4) {
      val nameLabel = new Label("placeholder")
      val nameText = new TextField
      val helloButton = new Button("Hello")
      val goodbyeButton = new Button("Goodbye")
      
      listenTo(helloButton, goodbyeButton)
      contents ++= nameLabel :: nameText :: helloButton :: goodbyeButton :: Nil
      
      reactions += {
        case ButtonClicked(helloButton) =>
          Dialog.showMessage(nameLabel, "Hello " + nameText.text)
        //case ButtonClicked(goodbyeButton) =>
          //Dialog.showMessage(nameLabel, "Goodbye " + nameText.text)
      }
      
    }
    
  }
}

