/**
 * ScalaTextEditor
 *
 * #####Developers#####
 * #Daniel Latham
 * #Stephen Stucky
 */

/**
 * Singleton obj to initiate and initialize all other objects in this program
 */

object ScTextEditor {

  def main(args: Array[String]){
    
    println("code.")
    println(stephenWasHere())
    
  }

  def stephenWasHere() = "Stephen was here"

}
