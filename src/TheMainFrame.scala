import scala.swing._
import Swing._


class TheMainFrame() extends SimpleSwingApplication {
  def top = new MainFrame {
    title = "Sext Ed: The Scala Text Editor."
    
    //creating the menu bar.
    menuBar = new MenuBar {
      contents += new Menu("File") {
        //File menu contents.
        contents += new MenuItem(Action("New File") {
          println(title + "New File clicked")
        })
        
        contents += new MenuItem(Action("Save") {
          println(title + "Save clicked")
        })
        
        contents += new MenuItem(Action("Save As...") {
          println(title + "Save As... clicked")
        })
        
      } //end Filemenu.
      
      contents += new Separator
      
      contents += new MenuItem(Action("Find/Replace") {
        println(title + "Fine/Replace clicked")
      })
      
      

      
    } //end menuBar
    
    contents = new FlowPanel {
      contents += new TextArea()
      }
      
    }
    
  // end mainFrame
  
}